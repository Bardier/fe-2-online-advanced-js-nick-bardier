// * ------------------------------------------------------------
// * Воросы
// * ------------------------------------------------------------

// Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

// Когда у нас есть возможность, что что-то пойдет не так по коду и есть вероятность схлопотать ошибку.
// Чаще всего используется при работе с бэком. Нам к примеру должен прийти массив от бэка а приходит объект.
// Мы пробуем с ним работать как с массивом и улетаем в ошибку и тут ахтунг все приложение лежит и не двигается.
// А так мы обернули все это в try/catch и красота )
// P.S. Хотя и там уже не особо используется, замена promise и его .then() .catch() .finally()

// * ------------------------------------------------------------
// * Задания
// * ------------------------------------------------------------

// Дано масив books.
// Виведіть цей масив на екран у вигляді списку (тег ul – список має бути згенерований за допомогою Javascript).
// На сторінці повинен знаходитись div з id="root", куди і потрібно буде додати цей список (схоже завдання виконувалось в модулі basic).
// Перед додаванням об'єкта на сторінку потрібно перевірити його на коректність (в об'єкті повинні міститися всі три властивості - author, name, price).
// Якщо якоїсь із цих властивостей немає, в консолі має висвітитися помилка із зазначенням - якої властивості немає в об'єкті.
// Ті елементи масиву, які не є коректними за умовами попереднього пункту, не повинні з'явитися на сторінці.

const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

const root = document.querySelector("#root");
const booksList = document.createElement("ul");

books.forEach((book, i) => {
  try {
    if (!book.hasOwnProperty("author")) {
      throw new Error(`Нет автора в ${i + 1}й книге`);
    }
    if (!book.hasOwnProperty("name")) {
      throw new Error(`Нет названия в ${i + 1}й книге`);
    }
    if (!book.hasOwnProperty("price")) {
      throw new Error(`Нет цены в ${i + 1}й книге`);
    }

    const { author, name, price } = book;

    const li = document.createElement("li");
    const authorParagraph = document.createElement("p");
    const nameParagraph = document.createElement("p");
    const priceParagraph = document.createElement("p");

    authorParagraph.textContent = `Автор: ${author}`;
    nameParagraph.textContent = `Название: ${name}`;
    priceParagraph.textContent = `Цена: ${price}`;

    li.append(authorParagraph, nameParagraph, priceParagraph);

    booksList.append(li);
  } catch (error) {
    console.log(error);
  }
});

root.append(booksList);
