import { sendPost } from "./services.js";
import Card from "./Card.js";

export default class Modal {
  constructor({ headerTitle }) {
    this.headerTitle = headerTitle;
  }

  closeModal() {
    this.modal.addEventListener("click", (e) => {
      if (
        e.target.classList.contains("modal") ||
        e.target.classList.contains("close-btn")
      ) {
        this.modal.remove();
        document.body.classList.remove("modal-open");
      }
    });
  }

  createNewPost() {
    this.modal.querySelector(".modal__form").addEventListener("submit", (e) => {
      e.preventDefault();

      const title = this.modal.querySelector("input").value;
      const body = this.modal.querySelector("textarea").value;
      if (!title || !body) {
        alert("Заполните форму!");
        return;
      }

      const userId = 1;
      const author = "Leanne Graham";
      const email = "sincere@april.biz";
      const allCards = [...document.querySelectorAll(".card")];
      const cardID =
        Math.max(...allCards.map((el) => el.getAttribute("data-post-id"))) + 1;

      const card = new Card({
        title,
        text: body,
        author,
        email,
        cardID,
      });

      document
        .querySelector(".cards-list")
        .insertAdjacentElement("afterbegin", card.render());

      sendPost({
        cardID,
        userId,
        title,
        body,
      }).then(console.log);

      this.modal.remove();
      document.body.classList.remove("modal-open");
    });
  }

  render() {
    document.body.classList.add("modal-open");

    this.modal = document.createElement("div");
    this.modal.classList.add("modal");
    this.modal.innerHTML = `
			<div class="modal__content">
				<h2 class="modal__title">${this.headerTitle}</h2>
				<form class="modal__form">
					<label class="modal__label">Title: <input type="text" /></label>
					<label class="modal__label">Text: <textarea></textarea></label>
					<div class="modal__btns-wrapper">
						<button class="btn" type="submit">Add post</button>
						<button class="btn close-btn" type="button">Cansel</button>
					</div>
				</form>
			</div>
		`;

    this.createNewPost();
    this.closeModal();

    return this.modal;
  }
}
