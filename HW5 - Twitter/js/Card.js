export default class Card {
  constructor({ title, text, author, email, cardID, userID }) {
    this.title = title;
    this.text = text;
    this.author = author;
    this.email = email;
    this.cardID = cardID;
    this.userID = userID;
  }

  render() {
    this.card = document.createElement("li");
    const cardTitle = document.createElement("h2");
    const cardText = document.createElement("p");
    const cardAuthor = document.createElement("p");
    const cardAuthorEmail = document.createElement("p");
    const cardBtnsWrapper = document.createElement("div");
    const cardBtnEdit = document.createElement("button");
    const cardBtnSave = document.createElement("button");
    const cardBtnDelete = document.createElement("button");

    this.card.setAttribute("data-post-id", this.cardID);
    this.card.setAttribute("data-user-id", this.userID);

    this.card.classList.add("card");
    cardTitle.classList.add("card__title");
    cardText.classList.add("card__text");
    cardAuthor.classList.add("card__author");
    cardAuthorEmail.classList.add("card__author-email");
    cardBtnsWrapper.classList.add("btns-wrapper");
    cardBtnEdit.classList.add("btn", "card__edit-btn");
    cardBtnSave.classList.add("btn", "card__save-btn");
    cardBtnDelete.classList.add("btn", "card__delete-btn");

    cardTitle.innerText = this.title;
    cardText.innerText = this.text;
    cardAuthor.innerHTML = `<span>author:</span> ${this.author}`;
    cardAuthorEmail.innerHTML = `<span>email:</span>
		<a href="mailto:${this.email}">${this.email}</a>`;
    cardBtnEdit.innerText = "Edit";
    cardBtnSave.innerText = "Save";
    cardBtnDelete.innerText = "Delete";

    cardBtnsWrapper.append(cardBtnEdit, cardBtnSave, cardBtnDelete);
    this.card.append(
      cardTitle,
      cardText,
      cardAuthor,
      cardAuthorEmail,
      cardBtnsWrapper
    );

    return this.card;
  }
}
