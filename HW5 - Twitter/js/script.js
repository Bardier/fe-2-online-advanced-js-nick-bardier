// * ------------------------------------------------------------
// * Задания
// * ------------------------------------------------------------

// Створити сторінку, яка імітує стрічку новин соціальної мережі Twitter.
// Технічні вимоги:
// При відкритті сторінки необхідно отримати з сервера список всіх користувачів та загальний список публікацій. Для цього потрібно надіслати GET запит на наступні дві адреси:
// https://ajax.test-danit.com/api/json/users
// https://ajax.test-danit.com/api/json/posts
// Після завантаження всіх користувачів та їх публікацій необхідно відобразити всі публікації на сторінці.
// Кожна публікація має бути відображена у вигляді картки (приклад: https://prnt.sc/q2em0x),
// та включати заголовок, текст, а також ім'я, прізвище та імейл користувача, який її розмістив.
// На кожній картці повинна бути іконка або кнопка, яка дозволить видалити цю картку зі сторінки.
// При натисканні на неї необхідно надіслати DELETE запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.
// Після отримання підтвердження із сервера (запит пройшов успішно), картку можна видалити зі сторінки, використовуючи JavaScript.
// Більш детальну інформацію щодо використання кожного з цих зазначених вище API можна знайти тут.
// Цей сервер є тестовим. Після перезавантаження сторінки всі зміни, які надсилалися на сервер, не будуть там збережені. Це нормально, все так і має працювати.
// Картки обов'язково мають бути реалізовані у вигляді ES6 класів. Для цього необхідно створити клас Card. При необхідності ви можете додавати також інші класи.
// Необов'язкове завдання підвищеної складності
// Поки з сервера під час відкриття сторінки завантажується інформація, показувати анімацію завантаження.
// Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
// Додати зверху сторінки кнопку Додати публікацію. При натисканні на кнопку відкривати модальне вікно, в якому користувач зможе ввести заголовок та текст публікації.
// Після створення публікації дані про неї необхідно надіслати в POST запиті на адресу: https://ajax.test-danit.com/api/json/posts.
// Нова публікація має бути додана зверху сторінки (сортування у зворотному хронологічному порядку). Автором можна присвоїти публікації користувача з id: 1.
// Додати функціонал (іконку) для редагування вмісту картки.
// Після редагування картки для підтвердження змін необхідно надіслати PUT запит на адресу https://ajax.test-danit.com/api/json/posts/${postId}.

import { createPostsArr, deletePost, putPost } from "./services.js";
import Modal from "./Modal.js";

const root = document.querySelector("#root");
const spinner = document.querySelector(".spinner");

const renderCards = (posts) => {
  const cardsList = document.createElement("ul");
  cardsList.classList.add("cards-list");

  posts.forEach((card) => {
    cardsList.append(card.render());
  });

  root.append(cardsList);

  deleteCard();
  editCard();
};

const deleteCard = () => {
  document.querySelector(".cards-list").addEventListener("click", (e) => {
    if (e.target.classList.contains("card__delete-btn")) {
      const cardForDelete = e.target.closest(".card");
      deletePost(cardForDelete.getAttribute("data-post-id")).then(console.log);
      cardForDelete.remove();
    }
  });
};

const addCard = () => {
  const addCardBtn = document.createElement("button");
  addCardBtn.classList.add("btn", "add-card-btn");
  addCardBtn.innerText = "Add post";
  root.append(addCardBtn);

  addCardBtn.style.display = "block";
  addCardBtn.addEventListener("click", () => {
    const modal = new Modal({
      headerTitle: "Add new post.",
    });

    document.body.append(modal.render());
  });
};

const editCard = () => {
  let card, cardEditBtn, cardSaveBtn, postId, userId, title, body;

  root.addEventListener("click", ({ target }) => {
    if (target.classList.contains("card__edit-btn")) {
      card = target.closest(".card");
      cardEditBtn = card.querySelector(".card__edit-btn");
      cardSaveBtn = card.querySelector(".card__save-btn");
      postId = card.getAttribute("data-post-id");
      userId = card.getAttribute("data-user-id");
      title = card.querySelector(".card__title");
      body = card.querySelector(".card__text");

      cardEditBtn.style.display = "none";
      cardSaveBtn.style.display = "inline-block";

      title.innerHTML = `
				<input
				type="text"
				value="${title.innerText}"
				/>
				`;
      body.innerHTML = `<textarea>${body.innerText}</textarea>`;
    } else if (target.classList.contains("card__save-btn")) {
      cardEditBtn.style.display = "inline-block";
      cardSaveBtn.style.display = "none";

      title.innerHTML = title.querySelector("input").value;
      body.innerHTML = body.querySelector("textarea").value;

      putPost(postId, {
        id: postId,
        body: body.innerText,
        title: title.innerText,
        userId,
      }).then(console.log);
    }
  });
};

createPostsArr().then((posts) => {
  renderCards(posts);
  spinner.style.display = "none";
  addCard();
});
