import Card from "./Card.js";

const API = "https://ajax.test-danit.com/api/json/";

const sendRequest = async (url, method = "GET", config) => {
  return await fetch(url, {
    method,
    body: config,
  }).then((response) => {
    if (response.ok) {
      if (method === "DELETE") {
        return response;
      } else {
        return response.json();
      }
    } else {
      return new Error("Что-то пошло не так!");
    }
  });
};

const getAuthors = () => sendRequest(`${API}users`);
const getPosts = () => sendRequest(`${API}posts`);
const deletePost = (id) => sendRequest(`${API}posts/${id}`, "DELETE");
const sendPost = (config) =>
  sendRequest(`${API}posts`, "POST", JSON.stringify(config));
const putPost = (id, config) =>
  sendRequest(`${API}posts/${id}`, "PUT", JSON.stringify(config));

const createPostsArr = async () => {
  const cardsArr = [];

  await Promise.all([getAuthors(), getPosts()]).then((data) => {
    const [authorsArr, postsArr] = data;

    authorsArr.forEach((author) => {
      postsArr.forEach((post) => {
        if (author.id === post.userId) {
          const card = new Card({
            title: post.title,
            text: post.body,
            author: author.name,
            email: author.email,
            cardID: post.id,
            userID: author.id,
          });
          cardsArr.push(card);
        }
      });
    });
  });
  return cardsArr;
};

export { createPostsArr, deletePost, sendPost, putPost };
