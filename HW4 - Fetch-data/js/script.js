// * ------------------------------------------------------------
// * Воросы
// * ------------------------------------------------------------

// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.

// Используется для получения / отправки каких либо данных с сервера и дальнейшей работы с ними.

// * ------------------------------------------------------------
// * Задания
// * ------------------------------------------------------------

// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані.
// Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

// Необов'язкове завдання підвищеної складності
// Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку.
// Бажано знайти варіант на чистому CSS без використання JavaScript.

const API = "https://ajax.test-danit.com/api/swapi/films";
const root = document.querySelector("#root");
const charactersArr = [];

const renderFilms = (films) => {
  const filmsList = document.createElement("ul");
  filmsList.classList.add("films");

  films.forEach(({ episodeId, name, openingCrawl, characters }) => {
    charactersArr.push(characters);
    filmsList.innerHTML += `
			<li class="films__card">
				<h2 class="films__name">${name}</h2>
				<p class="films__episode">Episode: ${episodeId}</p>
				<p class="films__descr">
					${openingCrawl}
				</p>
				<div class="actors">
					<div class="lds-spinner spinner">
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
						<div></div>
					</div>
				</div>
			</li>
		`;
  });

  getCharacters(charactersArr);

  root.append(filmsList);
};

const renderCharacters = (charactersArr, actors, index) => {
  const actorsList = document.createElement("ul");
  actorsList.classList.add("actors__list");

  charactersArr.forEach(({ name }) => {
    actorsList.innerHTML += `
						<li class="actors__name">${name}</li>
					`;
  });

  actors[index].innerHTML = `<h2 class="actors__title">Actors:</h2>`;
  actors[index].append(actorsList);
};

const getFilms = (url) => {
  fetch(url)
    .then((response) => response.json())
    .then(renderFilms)
    .catch((err) => alert(err.message));
};

const getCharacters = (charactersArr) => {
  charactersArr.forEach((characters, index) => {
    const promiceArr = [];

    characters.forEach((characterUrl) => {
      promiceArr.push(fetch(characterUrl).then((response) => response.json()));
    });

    Promise.all(promiceArr)
      .then((charactersArr) => {
        const actors = document.querySelectorAll(".actors");
        renderCharacters(charactersArr, actors, index);
      })
      .catch((err) => alert(err.message));
  });
};

getFilms(API);
