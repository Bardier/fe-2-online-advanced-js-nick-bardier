class Game {
  constructor() {
    this.table = document.createElement("table");
    this.cells = [];
    this.leftCells = [];
    this.levelSelect = document.createElement("select");
    this.stopBtn = document.createElement("button");
    this.startBtn = document.createElement("button");
    this.count = {
      player: 0,
      computer: 0,
    };
    this.interval = null;
    this.clickTime = null;
  }

  clickOnSell({ target }) {
    if (target.classList.contains("blue")) {
      target.classList.replace("blue", "green");
      this.count.player++;
    }
    if (this.count.player >= 51) {
      `You win, with count: ${this.count.player} - ${this.count.computer}`;
      this.stopGame();
    }
    console.log("Player: ", this.count.player);
  }

  startGame() {
    this.startBtn.disabled = true;
    this.cells.forEach((cell) => {
      cell.classList.remove("blue", "red", "green");
      cell.classList.add("white");
    });

    this.interval = setInterval(() => {
      if (this.leftCells.length) {
        const id = Math.floor(Math.random() * (this.leftCells.length - 0) - 0);
        const td = this.leftCells[id];
        td.classList.replace("white", "blue");

        this.clickTime = null;
        this.clickTime = setTimeout(() => {
          if (td.classList.contains("blue")) {
            td.classList.replace("blue", "red");
            this.count.computer++;
            console.log("Computer: ", this.count.computer);
          }
          if (this.count.computer >= 51) {
            alert(
              `You failed, with count: ${this.count.player} - ${this.count.computer}`
            );
            this.stopGame();
          }
        }, this.levelSelect.value);

        this.leftCells = [
          ...this.leftCells.slice(0, id),
          ...this.leftCells.slice(id + 1),
        ];
      }
    }, this.levelSelect.value);
  }

  stopGame() {
    this.startBtn.disabled = false;
    clearInterval(this.interval);
    this.leftCells = [...this.cells];
    this.count.player = this.count.computer = 0;
  }

  render() {
    for (let i = 0; i < 10; i++) {
      const row = document.createElement("tr");
      for (let j = 0; j < 10; j++) {
        const td = document.createElement("td");
        td.classList.add("white");
        this.cells.push(td);
        this.leftCells.push(td);
        row.append(td);
      }
      this.table.append(row);
    }

    this.levelSelect.innerHTML = `
			<option value="1500">Легкий</option>
			<option value="1000">Средний</option>
			<option value="500">Тяжелый</option>
		`;

    this.startBtn.textContent = "Начать игру!";
    this.startBtn.addEventListener("click", this.startGame.bind(this));
    this.stopBtn.textContent = "Стоп игра!";
    this.stopBtn.addEventListener("click", this.stopGame.bind(this));

    const wrapper = document.createElement("div");
    wrapper.classList.add("wrapper");
    wrapper.append(this.levelSelect, this.startBtn, this.stopBtn);

    document.querySelector("#root").append(wrapper, this.table);
    window.addEventListener("mousedown", this.clickOnSell.bind(this));
  }
}

const game = new Game();
game.render();
