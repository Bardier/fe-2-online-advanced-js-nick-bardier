// * ------------------------------------------------------------
// * Воросы
// * ------------------------------------------------------------

// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// Есть код синхроный, который выполняется строка за строкой и при этом никого нигде не ждет.
// А есть код асинхроный который выполняется паралеоьно синхроному. Самый простой простой вариант: запрос на сервер.

// * ------------------------------------------------------------
// * Задания
// * ------------------------------------------------------------

// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const btn = document.querySelector("button");
const out = document.querySelector(".out");

const sendRequest = async (url) => {
  return await fetch(url).then((response) => {
    if (response.ok) {
      return response.json();
    }
    return new Error("Что-то пошло не так!");
  });
};

const getIp = () => sendRequest("https://api.ipify.org/?format=json");
const getDataIp = (ip) =>
  sendRequest(`http://ip-api.com/json/${ip}?fields=1589273`);

btn.addEventListener("click", () => {
  getIp().then(({ ip }) => {
    getDataIp(ip).then((data) => renderOut(ip, data));
  });
});

const renderOut = (ip, { continent, country, city, regionName, district }) => {
  out.innerHTML = `
		<h2>Result by ip ${ip}:</h2>
		<div>Continent: ${continent}</div>
		<div>Country: ${country}</div>
		<div>City: ${city}</div>
		<div>Region name: ${regionName}</div>
		<div>District: ${district ? district : "сори... район API вернул пустым"}</div>
	`;
};
