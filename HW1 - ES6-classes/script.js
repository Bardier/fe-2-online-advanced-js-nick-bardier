// * ------------------------------------------------------------
// * Воросы
// * ------------------------------------------------------------

// Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript

// Прототипное наследование дает возможность получить свойства и методы одного объекта от другого.
// Допустим есть обьект user {...} и есть uasya {...},
// если нам надо чтоб uasya что-то умел что умеет user мы делаем его прототипом от user.
// Теперь все свойства и методы что есть у user есть и у обьекта uasya

// Для чого потрібно викликати super() у конструкторі класу-нащадка?

// Чтоб передать атрибуты родительскому классу, а точнее его конструктору.

// * ------------------------------------------------------------
// * Задания
// * ------------------------------------------------------------

// Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
// Створіть гетери та сеттери для цих властивостей.
// Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
// Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
// Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.

class Employee {
  constructor({ name, age, salary }) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }

  get age() {
    return this._age;
  }
  set age(value) {
    this._age = value;
  }

  get salary() {
    return this._salary;
  }
  set salary(value) {
    this._salary = value;
  }
}

class Programmer extends Employee {
  constructor({ name, age, salary, lang }) {
    super({ name, age, salary });
    this._lang = lang;
  }

  get lang() {
    return this._lang;
  }
  set lang(value) {
    this._lang = value;
  }

  get salary() {
    return this._salary * 3;
  }
}

const nick = new Programmer({
  name: "Nick",
  age: 37,
  salary: 700,
  lang: ["JS", "TypeScript", "React"],
});

const nastya = new Programmer({
  name: "Anastasiya",
  age: 27,
  salary: 450,
  lang: ["HTML", "CSS", "JS"],
});

console.log(nick);
console.log(nastya);
